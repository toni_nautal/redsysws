<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 12/12/17
 * Time: 16:24
 */

namespace AppBundle\Controller;


use AppBundle\Redsys\CreditCard;
use AppBundle\Redsys\Redirection;
use AppBundle\Redsys\Transaction;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class PaymentController extends Controller
{
    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function paymentLandingAction(Request $request)
    {
        $csrf    = uniqid( bin2hex(random_bytes(6)) );
        $session = $request->getSession();
        $session->set('redsys-csrf',$csrf);

        return $this->render('AppBundle:redsys:landing.html.twig',['csrf' => $csrf]);
    }

    /**
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function paymentAction(Request $request)
    {
        $session = $request->getSession();
        $csrf    = $session->get('redsys-csrf');

        if($request->get('csrf') != $csrf ){
            throw new BadRequestHttpException();
        }
        try{

            $amount   = $request->get('amount');
            $pan      = $request->get('pan');
            $cvv      = $request->get('cvv');
            $provider = CreditCard::MASTERCARD;

            $month    = $request->get('month');
            $year     = $request->get('year');
            $expires  = \DateTime::createFromFormat('m/y', $month."/".$year );

            if(!$expires){
                throw new \Exception("expiration date is not valid");
            }

            $creditCard  = new CreditCard($pan, $expires, $cvv, $provider );
            $transaction = new Transaction( (float)$amount, 'EUR', "es", "0", "144490531");

            $redsys = $this->get('app.redsys');
            $operation = $redsys->createOperation($transaction, $creditCard);
            $response = $operation->request();

            return new JsonResponse($response);

/*
            $transaction = new Transaction( 157.25, 'EUR', "es", "0", "144490501");
            $transaction->useRedirection();

            $redirection = new Redirection(
                'https://dev.nautal.com/payment-notify',
                'https://dev.nautal.com/payment-success',
                'https://dev.nautal.com/payment-error'
            );

            $redsys    = $this->get('app.redsys');
            $operation = $redsys->createOperation($transaction);
            $operation->setRedirection($redirection);
            $formdata = $operation->request();

            return $this->render('@App/redsys/paymentForm.html.twig',$formdata);

*/
        }
        catch (\Throwable $exception){
            return new Response($exception->getMessage());
        }

    }


}