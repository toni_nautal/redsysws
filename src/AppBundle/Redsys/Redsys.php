<?php

namespace AppBundle\Redsys;

/**
 * Class Redsys
 * @package AppBundle\Redsys
 */
class Redsys
{
    const SIGNATURE_VERSION = 'HMAC_SHA256_V1';

    /** @var string  */
    private $wserver;
    /** @var string  */
    private $server;
    /** @var array  */
    private $currencies;
    /** @var array  */
    private $terminals;
    /** @var array  */
    private $secrets;
    /** @var array  */
    private $merchantCodes;
    /** @var array  */
    private $languages;
    /** @var RedsysHelper  */
    private $redsysHelper;
    /** @var CreditCard  */
    private $creditCard;
    /** @var Transaction  */
    private $transaction;
    /** @var  Redirection */
    private $redirection;

    /**
     * Redsys constructor.
     * @param array $config
     */
    public function __construct( RedsysHelper $redsysHelper, array $config)
    {
        $this->wserver       = $config['URLWS'];
        $this->server        = $config['URL'];
        $this->currencies    = $config['MERCHANTCURRENCY'];
        $this->terminals     = $config['TERMINAL'];
        $this->merchantCodes = $config['MERCHANTCODE'];
        $this->secrets       = $config['SECRET'];
        $this->languages     = $config['LANG'];
        $this->redsysHelper  = $redsysHelper;
    }

    /**
     * @param Transaction $transaction
     * @param CreditCard|null $creditCard
     * @return $this
     * @throws \Exception
     */
    public function createOperation(Transaction $transaction, CreditCard $creditCard = null)
    {
        if($transaction->type() != Transaction::CONFIRM_PAYMENT
            && $transaction->wservice()
            && $creditCard == null
        ){
            throw new RedsysException("For this transaction, credit card is needed");
        }

        $this->creditCard  = $creditCard;
        $this->transaction = $transaction;
        return $this;
    }

    /**
     * @param string $secure
     * @return array
     */
    public function request(string $secure = "NO_SECURE")
    {
        if( !$this->transaction->wservice() ){
            return $this->createFromParams($secure);
        }

        $request = $this->buildXml($secure);
        $client = new \SoapClient($this->wserver);
        $response = $client->trataPeticion( [ 'datoEntrada' => $request ] );

        $response = $this->validateResponse($response, $this->transaction->order());
        return $response;
    }

    /**
     * @return string
     */
    public function buildXml(string $secure)
    {
        $secret       = $this->secrets[$this->transaction->currency()];
        $operation    = $this->getParameters($secure);
        $operationXml = $this->redsysHelper->arrayToXml($operation, 'DATOSENTRADA');

        $request = [
            "DATOSENTRADA"        => $operation,
            "DS_SIGNATUREVERSION" => self::SIGNATURE_VERSION,
            "DS_SIGNATURE"        => $this->redsysHelper->createSignature($secret, $operationXml, $this->transaction->order())
        ];

        $request = $this->redsysHelper->arrayToXml($request, "REQUEST");
        return $request;
    }

    /**
     * @param string $secure
     * @param bool $redirection
     * @return array
     */
    public function getParameters( string $secure, bool $redirection = false)
    {
        $operation = [
            "DS_MERCHANT_MERCHANTCODE"    => $this->merchantCodes[$this->transaction->currency()],
            "DS_MERCHANT_AMOUNT"          => $this->transaction->amount() * 100,
            "DS_MERCHANT_ORDER"           => $this->transaction->order(),
            "DS_MERCHANT_TERMINAL"        => $this->terminals[$this->transaction->currency()][$secure],
            "DS_MERCHANT_CURRENCY"        => $this->currencies[$this->transaction->currency()],
            "DS_MERCHANT_TRANSACTIONTYPE" => $this->transaction->type()
        ];

        if( $this->transaction->type() == Transaction::CONFIRM_PAYMENT ){
            $operation["DS_MERCHANT_AUTHORISATIONCODE"] = $this->transaction->code();
        }
        elseif(!$redirection){
            $operation["DS_MERCHANT_PAN"]        = $this->creditCard->pan();
            $operation["DS_MERCHANT_EXPIRYDATE"] = $this->creditCard->expires()->format('ym');
            $operation["DS_MERCHANT_CVV2"]       = $this->creditCard->cvv();
        }

        if($redirection){
            $operation['DS_MERCHANT_URLOK']       = $this->redirection->success();
            $operation['DS_MERCHANT_URLKO']       = $this->redirection->error();
            $operation['DS_MERCHANT_MERCHANTURL'] = $this->redirection->notify();
        }
        return $operation;
    }

    /**
     * @param string $secure
     * @return array
     */
    public function createFromParams(string $secure)
    {
        $secret    = $this->secrets[$this->transaction->currency()];
        $params    = $this->getParameters($secure, true);
        $operation = $this->redsysHelper->createMerchantParameters($params);
        $signature = $this->redsysHelper->createSignature($secret, $operation, $this->transaction->order());

        return [
            "redsys_URL"            => $this->server,
            "Ds_SignatureVersion"   => self::SIGNATURE_VERSION,
            "Ds_MerchantParameters" => $operation,
            "Ds_Signature"          => $signature
        ];
    }

    /**
     * @param Redirection $redirection
     * @return $this
     */
    public function setRedirection(Redirection $redirection)
    {
        $this->redirection = $redirection;
        return $this;
    }

    /**
     * @param $response
     * @param $order
     * @return bool|mixed
     */
    public function validateResponse( \stdClass $response , string $order)
    {
        $xml      = simplexml_load_string($response->trataPeticionReturn, "SimpleXMLElement", LIBXML_NOCDATA);
        $json     = json_encode($xml);
        $response = json_decode($json,TRUE);

        $secret    = $this->secrets[$this->transaction->currency()];

        $cadena  = $response['OPERACION']['Ds_Amount'];
        $cadena .= $response['OPERACION']['Ds_Order'];
        $cadena .= $response['OPERACION']['Ds_MerchantCode'];
        $cadena .= $response['OPERACION']['Ds_Currency'];
        $cadena .= $response['OPERACION']['Ds_Response'];
        $cadena .= $response['OPERACION']['Ds_TransactionType'];
        $cadena .= $response['OPERACION']['Ds_SecurePayment'];

        $signature = $this->redsysHelper->createSignatureResponse($secret, $cadena, $order);

        $validResponse = false;
        if($signature == $response['OPERACION']['Ds_Signature']){
            $validResponse = $response;
        }
        return  $validResponse;
    }

}