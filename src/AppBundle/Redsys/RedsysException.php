<?php

namespace AppBundle\Redsys;

use Throwable;

/**
 * Class RedsysException
 * @package AppBundle\Redsys
 */
class RedsysException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}