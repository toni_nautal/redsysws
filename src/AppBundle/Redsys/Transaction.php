<?php

namespace AppBundle\Redsys;

/**
 * Class Transaction
 * @package AppBundle\Redsys
 */
class Transaction
{

    const DIRECT_PAYMENT   = 'A';
    const BLOCK_PAYMENT    = 'O';
    const CONFIRM_PAYMENT  = 'P';

    /** @var float  */
    private $amount;
    /** @var string  */
    private $currency;
    /** @var string  */
    private $language;
    /** @var null|string  */
    private $code;
    /** @var string  */
    private $type;
    /** @var  string */
    private $order;
    /** @var bool  */
    private $wservice = true;

    /**
     * Transaction constructor.
     * @param float $amount
     * @param string $currency
     * @param string $language
     * @param string $type
     * @param string|null $code
     */
    public function __construct(
        float $amount,
        string $currency,
        string $language,
        string $type,
        string $order,
        string $code = null
    )
    {
        $this->amount = $amount;
        $this->currency = $currency;
        $this->language = $language;
        $this->code = $code;
        $this->type = $type;
        $this->order = $order;
    }

    /**
     * @return float
     */
    public function amount(): float
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function currency(): string
    {
        return $this->currency;
    }

    /**
     * @return string
     */
    public function language(): string
    {
        return $this->language;
    }

    /**
     * @return null|string
     */
    public function code():string
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function type(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function order(): string
    {
        return $this->order;
    }

    /**
     * @return bool
     */
    public function wservice():bool
    {
        return $this->wservice;
    }

    /**
     * @return $this
     */
    public function useRedirection()
    {
        $this->wservice = false;
        return $this;
    }

}