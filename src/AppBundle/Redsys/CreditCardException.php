<?php

namespace AppBundle\Redsys;

use Throwable;

/**
 * Class CreditCardException
 * @package AppBundle\Redsys
 */
class CreditCardException extends \Exception
{
    /**
     * CreditCardException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}