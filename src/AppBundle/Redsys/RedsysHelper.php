<?php

namespace AppBundle\Redsys;

/**
 * Class RedsysHelper
 * @package AppBundle\Redsys
 */
class RedsysHelper
{
    /**
     * @param $data
     * @param $key
     * @return string
     */
    private function encrypt_3DES(string $data, string $key)
    {
        $iv = "\0\0\0\0\0\0\0\0";
        $data_padded = $data;
        if (strlen($data_padded) % 8) {
            $data_padded = str_pad($data_padded, strlen($data_padded) + 8 - strlen($data_padded) % 8, "\0");
        }
        $ciphertext = openssl_encrypt($data_padded, "DES-EDE3-CBC", $key, OPENSSL_RAW_DATA | OPENSSL_NO_PADDING, $iv);
        return $ciphertext;
    }

    /**
     * @return string
     */
    function arrayToJson($data)
    {
        $json = json_encode($data);
        return $json;
    }

    /**
     * @return string
     */
    function createMerchantParameters(array $operation)
    {
        // Se transforma el array de datos en un objeto Json
        $json = $this->arrayToJson($operation);
        // Se codifican los datos Base64
        return $this->encodeBase64($json);
    }

    /**
     * @param $input
     * @return string
     */
    function base64_url_encode($input)
    {
        return strtr(base64_encode($input), '+/', '-_');
    }

    /**
     * @param $data
     * @return string
     */
    function encodeBase64($data)
    {
        $data = base64_encode($data);
        return $data;
    }

    /**
     * @param $input
     * @return bool|string
     */
    function base64_url_decode($input)
    {
        return base64_decode(strtr($input, '-_', '+/'));
    }

    /**
     * @param $data
     * @return bool|string
     */
    function decodeBase64($data)
    {
        $data = base64_decode($data);
        return $data;
    }

    /**
     * @param $ent
     * @param $key
     * @return string
     */
    function mac256($ent,$key)
    {
        $res = hash_hmac('sha256', $ent, $key, true);//(PHP 5 >= 5.1.2)
        return $res;
    }


    /**
     * @param $data
     * @return bool|string
     */
    function getOrder($data)
    {
        $posPedidoIni = strrpos($data, "<DS_MERCHANT_ORDER>");
        $tamPedidoIni = strlen("<DS_MERCHANT_ORDER>");
        $posPedidoFin = strrpos($data, "</DS_MERCHANT_ORDER>");
        return substr($data,$posPedidoIni + $tamPedidoIni,$posPedidoFin - ($posPedidoIni + $tamPedidoIni));
    }

    /**
     * @param string $secret
     * @param string $data
     * @return string
     */
    function createSignature(string $secret, string $data, int $order)
    {
        $key = $this->decodeBase64($secret);
        $key = $this->encrypt_3DES($order, $key);
        $res = $this->mac256($data, $key);
        return $this->encodeBase64($res);
    }


    /**
     * @param string $secret
     * @param string $data
     * @param int $order
     * @return string
     */
    function createSignatureResponse(string $secret, string $data, int $order)
    {
        $key = $this->decodeBase64($secret);
        $key = $this->encrypt_3DES($order, $key);
        $res = $this->mac256($data, $key);
        return $this->encodeBase64($res);
    }

    /**
     * @param array $data
     * @param string $root
     * @return string
     */
    public function arrayToXml(array $data, string $root)
    {
        $xml = "<".$root.">";
        foreach ($data as $key => $value){
            if(is_array($value)){
                $xml .= $this->arrayToXml($value, $key);
            }
            else{
                $xml .= "<".$key.">".$value."</".$key.">";
            }
        }
        $xml .= "</".$root.">";
        return $xml;
    }

}