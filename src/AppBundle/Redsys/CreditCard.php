<?php

namespace AppBundle\Redsys;

/**
 * Class CreditCard
 * @package AppBundle\Redsys
 */
class CreditCard
{
    const MASTERCARD       = 0;
    const VISA             = 1;
    const VISA_ELECTRON    = 2;
    const MAESTRO          = 3;
    const AMERICAN_EXPRESS = 4;

    /** @var string  */
    private $pan;
    /** @var int  */
    private $cvv;
    /** @var \DateTime  */
    private $expiration;
    /** @var int  */
    private $cardProvider;

    /**
     * CreditCard constructor.
     * @param string $pan
     * @param \DateTime $expiration
     * @param int $cvv
     * @param int $cardProvider
     */
    public function __construct( string $pan, \DateTime $expiration, int $cvv, int $cardProvider)
    {
        $this->pan = $pan;
        $this->cvv = $cvv;
        $this->expiration = $expiration;
        $this->cardProvider = $cardProvider;
        $this->validateCreditCard();
    }

    /**
     * @throws CreditCardException
     */
    public function validateCreditCard()
    {
        $today = new \DateTime();
        if ($this->expiration < $today){
            throw new CreditCardException("The credit card is expired", 100);
        }
        if(!$this->isNumberCorrect()){
            throw new CreditCardException("The credit card number is not valid", 101);
        }
        if( strlen($this->cvv) != 3  ){
            throw new CreditCardException("The CVV code is not valid", 102);
        }

    }

    /**
     * @return bool
     */
    public function isNumberCorrect()
    {
        if( ! ctype_digit($this->pan) ){
            return false;
        }

        switch ($this->cardProvider){

            case self::MASTERCARD:
                    return (strlen($this->pan) === 16) ? true : false;
                break;

            case self::VISA:
                return ( strlen($this->pan) === 13 || strlen($this->pan) === 16) ? true : false;
                break;

            case self::VISA_ELECTRON:
                return ( strlen($this->pan) === 13 || strlen($this->pan) === 16) ? true : false;
                break;

            case self::MAESTRO:
                return ( strlen($this->pan) >= 12 &&  strlen($this->pan) <= 19 ) ? true : false;
                break;

            case self::AMERICAN_EXPRESS:
                return ( strlen($this->pan) >= 15 ) ? true : false;
                break;
        }
    }

    /**
     * @return string
     */
    public function pan()
    {
        return $this->pan;
    }

    /**
     * @return int
     */
    public function cvv()
    {
        return $this->cvv;
    }

    /**
     * @return \DateTime
     */
    public function expires()
    {
        return $this->expiration;
    }

    /**
     * @return int
     */
    public function provider()
    {
        return $this->cardProvider;
    }
}