<?php
/**
 * Created by PhpStorm.
 * User: antonio
 * Date: 13/12/17
 * Time: 16:16
 */

namespace AppBundle\Redsys;

/**
 * Class Redirection
 * @package AppBundle\Redsys
 */
class Redirection
{
    /** @var string  */
    private $notify;
    /** @var string  */
    private $success;
    /** @var string  */
    private $error;
    /** @var string  */
    private $tld;

    /**
     * Redirection constructor.
     * @param string $notify
     * @param string $success
     * @param string $error
     * @param string $tld
     */
    public function __construct(string $notify, string $success, string $error, string $tld = 'es' )
    {
        $this->notify  = $notify;
        $this->success = $success;
        $this->error   = $error;
        $this->tld     = $tld;
    }

    /**
     * @return string
     */
    public function notify(): string
    {
        return $this->notify;
    }

    /**
     * @return string
     */
    public function success(): string
    {
        return $this->success;
    }

    /**
     * @return string
     */
    public function error(): string
    {
        return $this->error;
    }

    /**
     * @return string
     */
    public function tld(): string
    {
        return $this->tld;
    }

}